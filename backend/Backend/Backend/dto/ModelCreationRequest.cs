﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Backend.dto
{
    public class ModelCreationRequest
    {
        public string name;
        public string license;
        public string[] tags;
        public string[] categories;
        public bool isPublished;
        public string description;
        public byte[] modelFile;
        public string options;
    }
}