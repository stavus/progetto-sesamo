﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm.aspx.cs" Inherits="Backend.WebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>
    <script type="text/javascript">
        var app = angular.module("sketchfabApp", []);
        app.controller("MainController", ["$scope", "MainService", function (scope, MainService) {
            var vm = this;
            vm.file = null;
            vm.name = "prova";
            vm.loading = false;
            vm.submitForm = function () {
                MainService.sendForm(vm.name, vm.file, vm.loading);
            };
            vm.fileSelected = function (element) {
                vm.file = element.files[0];
            };
        }])
        app.service("MainService", ["$http", function (http) {
            this.sendForm = function (name, file, loading) {
                try {
                    loading = true;
                    http({
                        url: 'http://localhost:64537/services/RestServiceImpl.svc/api/v1/upload/' + name,
                        method: 'Post',
                        data: file,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity

                    })
                        .success(function (data) {
                            loading = false;
                            alert(data);
                        })
                        .error(function (error) {
                            loading = false;
                            alert(error);
                        });
                } catch (e) {
                    loading = false;
                    console.log(e);
                }
            };
        }])
    </script>
</head>
<body ng-app="sketchfabApp">
    <form runat="server" ng-controller="MainController as mainController">
        <h1>Upload file to skechfab</h1>
        <label for="file">
            <input type="file" onchange="angular.element(this).scope().mainController.fileSelected(this)" />
        </label>
        <label for="name">
            <input type="text" ng-model="mainController.name" />
        </label>
        <button ng-click="mainController.submitForm()" type="button">send</button>
    </form>
</body>
</html>
