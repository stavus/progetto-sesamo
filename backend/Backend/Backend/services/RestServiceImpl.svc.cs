﻿using Backend.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Net.Http;
using Backend.utils;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Backend.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestServiceImpl" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestServiceImpl.svc or RestServiceImpl.svc.cs at the Solution Explorer and start debugging.
    public class RestServiceImpl : IRestServiceImpl
    {
        static string sketchfabBaseUrl = "https://api.sketchfab.com";
        static string modelCreationPath = "/v3/models";

        public Example GetExample(string name)
        {
            Example tmp = new Example();
            tmp.verb = "hello";
            tmp.name = name;
            return tmp;
        }

        public string GetMock(string name)
        {
            return "hello " + name;
        }


        public async System.Threading.Tasks.Task<string> UploadFileAsync(string name, Stream file)
        {


            var payload = new ModelCreationRequest
            {
                categories = new string[] { "ring" },
                license = "by", // https://api.sketchfab.com/v3/licenses => [0].slug
                tags = new string[] { "ring" },
                isPublished = false,
                description = "ring",
                modelFile = RawUtils.StreamToBytearray(file)
            };

            // Serialize our concrete class into a JSON String
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(payload));

            // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

            using (var httpClient = new HttpClient())
            {

                // Do the actual request and await the response
                var httpResponse = await httpClient.PostAsync(sketchfabBaseUrl + modelCreationPath, httpContent);

                // If the response contains content we want to read it!
                if (httpResponse.Content != null)
                {
                    var responseContent = await httpResponse.Content.ReadAsStringAsync();

                    return responseContent;
                    // From here on you could deserialize the ResponseContent back again to a concrete C# type using Json.Net
                }
            }

            return "false";
        }


    }
}
